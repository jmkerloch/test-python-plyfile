# Tests utilisation package plyfile pour création fichier .ply

Ce projet contient un script pour des tests de création de fichier .ply supportés par MDAL.

Les fichiers sont créés avec le package pip plyfile.

## Mise en place environnement

Pour lancer le script:

- mise en place d'un environnement virtuel

`python -m venv .venv`

- activation environnement

`source .venv/bin/activate`

- installation plyfile

`pip install -r requirements.txt`

## Résultat expérimentation

Des fichiers .ply ont bien pu être créés avec le package plyfile.

Ces fichiers sont correctement lus par MDAL dans QGIS.

:warning: Il n'a pas été possible d'ajouter une composante temporelle pour les fichiers .ply. Il semble que ceci ne soit pas supporté par le driver PLY de MDAL. Une issue a été postée sur GitHub : https://github.com/lutraconsulting/MDAL/issues/461

:warning: Lors de la lecture des fichiers par QGIS, une couche de données Bed elevation a automatiquement été ajouté par MDAL. Une issue a été postée sur GitHub : https://github.com/lutraconsulting/MDAL/issues/462
 
