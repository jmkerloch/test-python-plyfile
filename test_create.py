from typing import List
import numpy as np
import numpy.lib.recfunctions as rfn
from plyfile import PlyData, PlyElement

# Definition des vertices et des faces
vertices=np.array([(0. , 0., 0.),
                   (1. , 0., 0.),
                   (2. , 0., 0.), 
                   (0.5, 1., 0.),
                   (1.5, 1., 0.),
                   (1. , 2., 0.)
                   ],
                     dtype=[('X', '<f8'), ('Y', '<f8'), ('Z', '<f8')])

# Avec valeurs sur les faces
faces=np.array([([0, 1, 3],0.),([1, 4, 3],1.), ([1, 2, 4],2.), ([3, 4, 5],3.)], dtype=[('vertex_indices', '<u4', (3,)), ('test_data', '<u4')])

# Sans valeurs sur les faces
faces_no_values=np.array([[0, 1, 3],[1, 4, 3], [1, 2, 4], [3, 4, 5]])
faces_no_val_np = np.empty(len(faces_no_values), dtype=[('vertex_indices', '<u4', (3,))])
faces_no_val_np["vertex_indices"]=faces_no_values


# Création fichier sans valeur
el_vertice = PlyElement.describe(vertices, 'vertex')
el_face = PlyElement.describe(faces_no_val_np, 'face')
PlyData([el_vertice,el_face]).write('no_value_for_mesh.ply')

# Création fichier avec valeur
el_vertice = PlyElement.describe(vertices, 'vertex')
el_face = PlyElement.describe(faces, 'face')
PlyData([el_vertice,el_face]).write('value_for_mesh.ply')


# Lecture fichier sans valeur et écriture avec ajout de valeur
values = np.array([0,1,2,3], dtype=[('test_data', '<u4')])
plydata = PlyData.read('no_value_for_mesh.ply')
elements = []
el_vertice = plydata.elements[0]
el_face = plydata.elements[1]
merge = rfn.merge_arrays((el_face.data, values), flatten=True)
el_face = PlyElement.describe(merge, 'face')
PlyData([el_vertice,el_face]).write('value_added_for_mesh.ply')
